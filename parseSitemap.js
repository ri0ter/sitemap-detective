const parallelRequest = require('./parallelRequest');

let counter = 0;

let emptyQueueCallback = null;
let responseCallback = null;

let queue = 0;

const getSitemapFromContent = (content) => {
    const sitemaps = content.match(/<sitemap>\s*<loc>(.*?)<\/loc>\s*<\/sitemap>/g);
    return sitemaps ? sitemaps.map(item => item.match(/<loc>(.*?)<\/loc>/)[1]) : []
};

const getURLfromContent = (content) => {
    const sitemaps = content.match(/<url>[^s]*?<loc>(.*?)<\/loc>/g);
    return sitemaps ? sitemaps.map(item => item.match(/<loc>(.*?)<\/loc>/)[1]) : []
};

const onResponse = (url, response) => {
    counter++;
    
    queue--;
    parallelRequest.queueRequests(getSitemapFromContent(response.body), 0)
    .forEach((req) => {
        queue++;
        req.then(({url, response}) => {
            onResponse(url, response)
        })
        .catch(({url, error}) => {
            onError(url, error);
        });
    });
    
    if(responseCallback) {
        responseCallback(getURLfromContent(response.body), url);
    }
    
    if(!queue) {
        onEmptyQueue();
    }
};

const onError = (url, error) => {
    console.log(url, error);
}

const onEmptyQueue = () => {
    console.log(`Total sitemaps responses: ${counter}`);
    if(emptyQueueCallback) {
        emptyQueueCallback();
    }
}

module.exports = (url, options) => {
    emptyQueueCallback = options.emptyQueueCallback;
    responseCallback = options.responseCallback;
    
    parallelRequest.queueRequests(url, 0).forEach((req) => {
        queue++;
        req.then(({url, response}) => {
            onResponse(url, response)
        })
        .catch((error) => {
            onError(error);
        });
    });
}
