const program = require('commander');

const checkURL = require('./checkURL');
const parseSitemap = require('./parseSitemap');
const parallelRequest = require('./parallelRequest');

// main sitemap url
let sitemapEntry;
//const urls = [];
const invalidResponse = 0;

program
.version('0.1.0')
.usage('<sitemap_url>')
.option('-l, --requests-limit <n>', 'An integer argument', parseInt)
.option('-v, --verbose', 'Verbose mode')
.arguments('<sitemap_url>')
.action((sitemapUrl) => {
    sitemapEntry = sitemapUrl;
});
  
program.parse(process.argv);

if (typeof sitemapEntry === 'undefined') {
   console.error('no sitemap given!');
   process.exit(1);
}

const onSitemapTraversed = () => {
    console.log('onSitemapTraversed');
};

if(program.requestsLimit) {
    parallelRequest.limit = program.requestsLimit;
}

parseSitemap(sitemapEntry, {
    emptyQueueCallback: onSitemapTraversed,
    responseCallback: (url, sitemapUrl) => {
        if(!url) {
            return;
        }

        let invalidResponse = 0;
        
        new checkURL(url, {
            emptyQueueCallback: () => {
                console.log(
                    `Done checking: ${sitemapUrl}\nInvalidResponse ${invalidResponse} out of ${url.length}`
                );
            },
            responseCallback: (success) => {
                if(!success) {
                    invalidResponse++;
                }
            },
            errorCallback: (error) => {
                invalidResponse++;
            },
            verbose: program.verbose
        });
    }
});
