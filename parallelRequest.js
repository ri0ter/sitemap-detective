const request = require('request');

const DEFAULT_LIMIT = 16;

let _limit = DEFAULT_LIMIT;
let _inUse = 0;
let _queue = [];

const ParalellRequest = {

    set limit(value) {
        _limit = Math.min(Math.max(value || DEFAULT_LIMIT, 1), 32);
    },
    
    get limit() {
        return _limit;
    },
    
    get queue() {
        return _queue;
    },
    
    /*
     * Add requests to check
     */ 
    addToQueue(urls) {
        _queue =  urls.filter((item, index) => urls.indexOf(item) === index).concat(_queue).sort((a,b) => b.priority - a.priority);
    },

    /*
     * Call next request
     */
    next() {
        if(!_queue.length) {
            return;
        }
        
        let client = _queue.shift();
        client.trigger();
    },
    
    queueRequests (urls, priority) {
        priority = priority || 0;
        
        return this.process(
            [].concat(urls).map((url) => {
                return {
                    url,
                    priority
                }
            })
        )

    },
    
    /*
     * Process requests in queue
     */
    process(request) {
        const client = request.map((req) => this.prepare(req));
        
        this.addToQueue(client);
        for(let i = 0; i< (_limit - _inUse); i++) {
            this.next();
        }
        return client.map(client => client.promise);
    },
    
    /*
     * Prepare request for processing
     */
    prepare(req) {
        const url = req.url;
        
        let res;
        let rej;
        
        const promise = new Promise((resolve, reject) => {
            res = resolve;
            rej = reject;
        });
        
        const trigger = () => {
            _inUse++;
            request(url, (error, response) => {
                _inUse--;
                //this.processing = false;
                if(error) {
                    rej({url, error});
                }
                this.next();
                res({url, response});
            });
        };
        
        return {
            priority: req.priority,
            promise,
            trigger
        }
    }
}

module.exports = ParalellRequest;
