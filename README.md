# Sitemap detective

Verify your sitemap links.

## Instalation
Clone the repo, and type npm install.

## Usage
To run the app type `node index <sitemap_url>`.

### Options
```
-l --requests-limit limit of concurent requests during url check (dedefault is 8)
```

