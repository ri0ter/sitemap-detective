const parallelRequest = require('./parallelRequest');

const isValidResponse = (response) => {
    return response && response && response.statusCode === 200;
};
    
class CheckURL {
    constructor(urls, options) {
        this.processed = 0;

        const emptyQueueCallback = options.emptyQueueCallback;
        const responseCallback = options.responseCallback;
        const errorCallback = options.errorCallback;
        
        const onResponse = (url, response) => {
            const validResponse = isValidResponse(response);

            if(options.verbose) {
                if(validResponse) {
                    console.log(`\x1b[90m\u221A ${url}\x1b[0m`);
                }
                else {
                    console.log(`\x1b[90m\u00D7 ${url}\x1b[0m`);
                }
            }

            if(responseCallback) {
                responseCallback(validResponse);
            }

            if(this.processed === urls.length) {
                onEmptyQueue();
            }
        };

        const onError = (error) => {
            if(errorCallback) {
                errorCallback();
            }
        };
        
        const onEmptyQueue = () => {
            if(emptyQueueCallback) {
                emptyQueueCallback();
            }
        };
        
        const requests = parallelRequest.queueRequests(urls, 1);

        if(requests.length) {

            requests.forEach((request) => {
                request.then(({url, response}) => {
                    this.processed++;
                    onResponse(url, response);
                })
                .catch(({url, error}) => {
                    this.processed++;
                    if(options.verbose) {
                        console.log(`\x1b[90merror @ ${url}\x1b[0m`);
                        console.log(`\x1b[31m${error}\x1b[0m`);
                    }
                });
            });
        
        }
        else {
            onEmptyQueue();
        }

    }

}

module.exports = CheckURL
